# README #

This is ASP.NET Core MVC web application based on e-learning course from Pluralsight: "Building Your First ASP.NET Core 2 MVC Application" by Gill Cleeren.

### How do I get set up? ###

* Prerequisites: .NET Core 2.2 runtime, Visual Studio, MS SQL Server
* Download ZIP or clone repository,
* Open BethanysPieShop.sln
* Run and enjoy!

### Additional packages used in the project ###

* [Entity Framework](https://www.entityframeworktutorial.net/what-is-entityframework.aspx)